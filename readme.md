# XBASIC - Extended BASIC Compiler
This program will read an item to be BASIC compiled,
precompile it for a specific MV system, compile the updated
code, and rewrite the original code when complete.